---
layout: page
title: Java Development
permalink: /
---

# Java Development

<br/>

### [Derek Banas] Java Tutorial

<div align="center">
    <iframe width="853" height="480" src="https://www.youtube.com/embed/n-xAqcBCws4" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>

<br/>

### [Douglas Schmidt] CS 891 (2017) Introduction to Concurrent and Parallel Java

<div align="center">
    <iframe width="853" height="480" src="https://www.youtube.com/embed/NVcbxQEoseE" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>

<br/>

Also on channel:

<br/>

-   Pattern-Oriented Software Architectures for Concurrent and Networked Software
-   CS 892 (2017) Concurrent Java Programming with Android
-   CS 891 (Spring 2018) Advanced Concurrent Java Programming in Android
-   etc

<br/>

### Projects in Enterprise Java | Creating a Voting System (I think not full video course)

<div align="center">
    <iframe width="853" height="480" src="https://www.youtube.com/embed/aCOdp3hdy0c" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>

<br/>

### Docker for JAVA DEVELOPERS

<div align="center">
    <iframe width="853" height="480" src="https://www.youtube.com/embed/IgJXYU3GOM4" frameborder="0" allowfullscreen></iframe>
</div>

<br/>

So difficult, because Arun use Mac and IntelliJ for containers.

Some Examples:  
https://github.com/docker/labs/tree/master/developer-tools/java

If you know any interesting examples with docker and java, please send links. Especially if you have some examples with Weblogic.

I have java ee application what i can run on weblogic, but i didn't prepare containers for it.

### Free Book

By: Arun Gupta  
Released: June 2016

-   http://shop.oreilly.com/product/0636920050872.do

<!-- <br/><br/>

<div align="center">
    <img src="http://storage4.static.itmages.ru/i/16/0719/h_1468924764_4874600_196541fb75.png" border="0" alt="I can't open the jar">
</div> -->

<br/>

### Spring Boot Quick Start

<div align="center">

    <iframe width="853" height="480" src="https://www.youtube.com/embed/videoseries?list=PLqq-6Pq4lTTbx8p2oCgcAQGQyqN8XeA1x" frameborder="0" allowfullscreen></iframe>

</div>

<br/>

### Docker and Kubernetes Recipes for Java Developers by Arun Gupta

<div align="center">

    <iframe width="853" height="480" src="https://www.youtube.com/embed/aSATsLG59Zs" frameborder="0" allowfullscreen></iframe>

</div>

<br/>

### Refactor your Java EE application using Microservices and Containers by Arun Gupta

<div align="center">

    <iframe width="853" height="480" src="https://www.youtube.com/embed/iJVW7v8O9BU" frameborder="0" allowfullscreen></iframe>

</div>

<br/>

### Java 8 Lambda Basics

<div align="center">

    <iframe width="853" height="480" src="https://www.youtube.com/embed/videoseries?list=PLqq-6Pq4lTTa9YGfyhyW2CqdtW9RtY-I3" frameborder="0" allowfullscreen></iframe>

</div>

<a href="//labs.javadev.org/labs/lab-07/">I'm trying to study this course</a>

<br/>

### JavaOne 2016

<div align="center">

    <iframe width="853" height="480" src="https://www.youtube.com/embed/videoseries?list=PLPIzp-E1msrYicmovyeuOABO4HxVPlhEA" frameborder="0" allowfullscreen></iframe>

</div>

<br/>

### Java Memory Management

<div align="center">

    <iframe width="853" height="480" src="https://www.youtube.com/embed/ckYwv4_Qtmo" frameborder="0" allowfullscreen></iframe>

</div>

<br/>

### JVM ( java virtual machine) architecture - tutorial

<div align="center">

    <iframe width="853" height="480" src="https://www.youtube.com/embed/ZBJ0u9MaKtM" frameborder="0" allowfullscreen></iframe>

</div>

<br/>

### Robert Martin

**SOLID Principles of Object Oriented and Agile Design**

<br/>

<div align="center">
    <iframe width="853" height="480" src="https://www.youtube.com/embed/TMuno5RZNeE" frameborder="0" allowfullscreen></iframe>
</div>

<br/>

**The S.O.L.I.D. Principles of OO and Agile Design - by Uncle Bob Martin**

<br/>

<div align="center">
    <iframe width="853" height="480" src="https://www.youtube.com/embed/t86v3N4OshQ" frameborder="0" allowfullscreen></iframe>
</div>

<br/>

**Solid Principles by Uncle Bob Martin**

<br/>

<div align="center">
    <iframe width="853" height="480" src="https://www.youtube.com/embed/oar-T2KovwE" frameborder="0" allowfullscreen></iframe>
</div>

<br/>

**The Principles of Clean Architecture by Uncle Bob Martin**

<br/>

<div align="center">
    <iframe width="853" height="480" src="https://www.youtube.com/embed/o_TH-Y78tt4" frameborder="0" allowfullscreen></iframe>
</div>

https://www.youtube.com/watch?v=lZq8Jlq18Ec  
https://www.youtube.com/watch?v=yIf3EQmHmyw

<br/><br/>

-   DevExpress CTO Messages for the SOLID object oriented development Principles  
    https://www.youtube.com/playlist?list=PL4CE9F710017EA77A

<br/><br/>

### Java tutorialspoint:

http://www.tutorialspoint.com/java/java_tutorial.pdf

<br/><br/>

### Please share interesting code samples on java with us:

If you find interesting project on github, bitbucket, book, video course etc. on java, please send a link to us.

<br/><br/>

<strong>For example, next repositories is very interesting i think:</strong>

<br/><br/>

<a href="https://bitbucket.org/javadev-org/java-design-patterns/src/master/">Design Patterns</a><br/>

<br/><br/>

![I can't open the jar](/img/jars.jpg "I can't open the jar"){: .center-image }

<br/><br/>

### LABS

**I decided to move list of books and video courses about java development on <a href="//labs.javadev.org/">labs.javadev.org</a>**

### If you planning to watch any Video Course or read any book about Java Development and want to discuss it, we can create discussion board on javalabs.org

GitHub can help to us. We can create issue, discuss, send pull request to improve code and etc.
If it would be interesting to you, send link on course what you planning to study and link to your github || bitbucket repo.

For example github on video course:

<br/>

[Udemy] Building An E-Commerce Store Using Java Spring Framework [ENG, 2016] <br/>
https://bitbucket.org/marley-spring/building-an-e-commerce-store-using-java-spring-framework/src/master/

My contact: <br/>

![Marley](/img/a3333333mail.gif "Marley"){: .center-image }

<br/><br/>

![WTF in Minutes](/img/java_wtf.jpg "WTF in Minutes"){: .center-image }
