---
layout: page
title: Site Map
permalink: /sitemap/
---


# SITEMAP:

<br/>

### Development Tools:

<ul>
    <li><strong><a href="/devtools/">Development Tools</a></strong></li>
</ul>


<br/>

### Application Servers

<ul>
    <li><strong><a href="/appserv/">Application Servers</a></strong></li>
</ul>


<br/>

### Java Messaging

<ul>
    <li><strong><a href="/java_basics/installation/activemq/centos/6/x86_x64/">ActiveMQ installation on Centos 6.X x86_x64</a></strong></li>
</ul>


<br/>

### Docker:

<ul>
    <li><strong><a href="/docker/">Docker</a></strong></li>
</ul>

<br/>

### Android

<ul>
    <li><strong><a href="/java_basics/android/installation/">Prepare Ubuntu Linux Environment for Development Android Apps with Android Studio</a></strong></li>
</ul>



<br/>

### Examples:

<ul>
    <li><strong><a href="/examples/jboss-installation-on-windows/">Jboss6 Installation on Windows</a></strong>  </li>
    <li><strong><a href="/examples/eclipse_helios_and_jboss6/">Eclipse Helios and Jboss6</a></strong></li>
    <li><strong><a href="/examples/simple_java_project_with_jsf_and_facelets/">Simple java project with jsf and facelets</a></strong></li>
    <li><strong><a href="/examples/eclipse_helios_and_weblogic/">Eclipse Helios and Oracle Weblogic</a></strong></li>
</ul>
