---
layout: page
title: Java Basics Language Loops
permalink: /lang/basics/convert-types/
---

# [Java Basics] Convert Types


### Long to String

    long b = 12L;
    System.out.println(String.valueOf(b));   


<br/>

### String to Long

    Long.parseLong("999");
