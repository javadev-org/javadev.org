---
layout: page
title: Application Servers
permalink: /appserv/
---


<br/>

### TomCat:

<strong><a href="/docs/appserv/tomcat/installation/windows/">TomCat Installation on Windows</a></strong>

<br/>

### jBoss / WildFly:

<strong><a href="/docs/appserv/wildfly/8.2/installation/">Wildfly 8.2 Installation on Centos 6.6 x86_64</a></strong>

<strong><a href="/appservers/wildfly/8.2/jdbc/postgresql/">Wildfly 8.2 setup jdbc connection with PostgreSQL</a></strong>

<strong><a href="/docs/appserv/wildfly/8.2/active-mq/">Configure a Resource Adapter for ActiveMQ on WildFly 8.2</a></strong>

<br/>

### Glassfish:

<strong><a href="/docs/appserv/centos/6.6/glassfish/4/installation/">Glassfish 4 Installation on Centos 6.6</a></strong>

<br/>

### Weblogic:

<strong><a href="/docs/appserv/weblogic/12c/installation/">Weblogic 12c Installation on Centos 6.6 x86_64</a></strong>
