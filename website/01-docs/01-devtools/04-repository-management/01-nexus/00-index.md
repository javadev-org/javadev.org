---
layout: page
title: Sonatype Nexus
permalink: /devtools/repository-management/nexus/
---


### Sonatype Nexus Repository Manager (Nexus)

login: admin  
password: admin123  


<strong><a href="/devtools/repository-management/nexus/docker/">Run Nexus in Docker container</a></strong>


<strong><a href="/devtools/repository-management/nexus/3/installation-on-linux/">Nexus 3.X Installation in linux</a></strong>


<strong><a href="/devtools/repository-management/nexus/2/installation-on-linux/">Nexus 2.X Installation in linux</a></strong>

<strong><a href="/devtools/repository-management/nexus/2/using-the-oracle-maven-repository-with-nexus/">Using the Oracle Maven Repository with Nexus 2</a></strong>
