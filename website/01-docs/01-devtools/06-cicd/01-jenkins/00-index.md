---
layout: page
title: Jenkins Installation on Centos 6.6
permalink: /devtools/cicd/jenkins/
---

# Jenkins Installation

### [Jenkins Installation in Ubuntu 20.04](/devtools/cicd/jenkins/setup/ubuntu/20.04/)

### [Jenkins Installation on Centos 6.6](/devtools/cicd/jenkins/setup/centos/6/)
