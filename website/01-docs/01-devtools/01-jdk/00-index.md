---
layout: page
title: Java (JDK) Installation
permalink: /devtools/jdk/setup/
---

# Java (JDK) Installation:

<ul>
    <li><strong><a href="http://www.guru99.com/install-java.html" rel="nofollow">JDK8 Installation on Windows</a></strong></li>
    <li><strong><a href="/devtools/jdk/setup/linux/">JDK installation in linux (Ubuntu, Centos)</a></strong></li>
</ul>

<br/>

**Archive:**

<br/>

<ul>
    <li><strong><a href="/devtools/jdk/setup/windows/xp/">JDK6 Installation on Windows XP</a></strong></li>
</ul>
