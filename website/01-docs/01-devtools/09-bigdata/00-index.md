---
layout: page
title: Java Development Tools for BigData
description: Java Development Tools for BigData
keywords: Java, BigData, tools
permalink: /devtools/bigdata/
---

# Java Development Tools for BigData

<br/>

### Hadoop

<ul>
    <li><strong><a href="/devtools/bigdata/hadoop/">Hadoop</a></strong></li>
</ul>


<br/>

### Apache Spark2

<ul>
    <li><strong><a href="/devtools/bigdata/spark/install/linux/">Install in linux</a></strong></li>
</ul>


<br/>

### Scala

<ul>
    <li><strong><a href="/devtools/bigdata/scala/install/linux/">Install in linux</a></strong></li>
</ul>


<br/>

### Kafka

<ul>
    <li><strong><a href="/devtools/bigdata/kafka/docker/">Run kafka in docker container</a></strong></li>
    <li><strong><a href="/devtools/bigdata/kafka/install/linux/">Install in linux</a></strong></li>
</ul>
