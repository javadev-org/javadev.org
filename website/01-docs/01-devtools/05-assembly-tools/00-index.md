---
layout: page
title: Assembly Tools
description: ssembly Tools
keywords: java, assembly, maven, gradle
permalink: /devtools/assembly-tools/
---

<br/>

# Assembly Tools

<br/>

### Maven

<ul>
    <li><a href="/devtools/assembly-tools/maven/linux/centos/7/">Maven installation in Centos 7</a></li>
</ul>

<br/>

### Gradle

<ul>
    <li><a href="/devtools/assembly-tools/gradle/linux/ubuntu/">Gradle installation in Ubuntu</a></li>
    <li><a href="/devtools/assembly-tools/gradle/linux/centos/7/">Gradle installation in Centos 7</a></li>
</ul>
